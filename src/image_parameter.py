import json
from pydantic import BaseModel
from typing import List
import setting


class BaseModelFileExtension(BaseModel):
    def save(self, file_name: str) -> None:
        with open(setting.PARAMETER_PATH + '\\' + file_name, "w") as file:
            json.dump(self.dict(), file)

    @classmethod
    def read(cls, file_name: str) -> None:
        with open(setting.PARAMETER_PATH + '\\' + file_name) as file:
            _dt = json.load(file)
            dt = TemplateData(**_dt)


class TemplateData(BaseModelFileExtension):
    average: List[int]
    amplitude: List[int]
    mean: int


_data = {
    "x": [10, 11],
    "y": [4, 3],
    "mean": 12,
}

data = TemplateData.parse_obj(_data)
data.save("ololo.json")

dt = TemplateData.parse_file("ololo.json")
print(dt)

if __name__ == '__main__':
    print("parameters")
