from PIL import Image

import setting

class CutImage:
    def __init__(self, fp: str) -> None:
        """
        Открытие шаблонных изображений
        fp - путь к файлу
        """
        print(fp)
        self.fp = fp
        self.image = Image.open(fp)

    def get_physical_data(self) -> float:
        """
        Получение физических данных изображения
        """
        w = 2 * setting.H * setting.ALPHA  # Нахождение плоскости изображения в сантиметрах
        N_width = self.image.size[1]  # Ширина изображения ( 6000х4000 - будет 6000 )
        R_w = w / N_width  # Количество пикселей в одном сантиметре
        return R_w

    def cut(self, size: int) -> None:
        """
        Обрезки изображения
        size - размер блока в метрах
        """
        pixel = self.get_physical_data()
        K_px = size * 10 / pixel
        for i in range(0, self.image.size[0], 50):
            for j in range(0, self.image.size[1], 50):
                img_crop = self.image.crop((i, j, i+50, j+50))
                img_crop.save(setting.IMAGE_CUT_PATH + "\\" + str(i) + 'x' + str(j) + '.png')
