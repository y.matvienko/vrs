import os

"""
Const paths
"""
IMAGE_PATH = os.getcwd() + "/static/images"
IMAGE_ORIGINAL_PATH_JPG = IMAGE_PATH + "/original_images/jpg"
IMAGE_ORIGINAL_PATH_TIF = IMAGE_PATH + "/original_images/tif"
IMAGE_CUT_PATH = IMAGE_PATH + "/cut_images"
IMAGE_MERGE_PATH = IMAGE_PATH + "/merge_images"
IMAGE_FIGURE_SAVE_PATH = "./static/images/save_figures/saved_figure_"
PARAMETER_PATH = "./static/parameters"
TEMPLATES_PATH = "./static/templates"

"""
Extension path
"""
JPG = ".JPG"
TIFF = ".TIFF"
PNG = ".PNG"

"""
One Drone Map
"""
RUN_DOCKER_ODM = "docker run -ti --rm -v C:/Users/Grava/Desktop/datasets:/datasets opendronemap/odm --project-path /datasets project --fast-orthophoto"

"""
Info Image
"""
SIZE_MATRIX = 23.5 * 16.5
FOCUS = 16
H = 268
ALPHA = 0.734
