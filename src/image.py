import os
import setting
import shutil
from PIL import Image


class ImageInfo:
    """
    Информация об ихображении
    """

    def __init__(self, original_path):
        self.original_path = original_path

    def count_images(self):
        """
        Количество изображений
        """
        list_images = os.listdir(self.original_path)
        return len(list_images)

    def name_images(self):
        """
        Наименование файла изображения
        """
        return os.listdir(self.original_path)

    def is_equal(self):
        """
        Количество фотографий в static соответствует в ODM?
        """
        count = 0
        for image in self.name_images():
            if image.find(setting.JPG.upper()) != -1:
                count += 1
        return self.count_images() == count

    def copy(self):
        """
        Копирование оригинальной фотографии в ODM
        """
        if self.is_equal():
            for image in self.name_images():
                shutil.copy(setting.IMAGE_ORIGINAL_PATH_JPG + "\\" +
                            image, setting.ONE_DRONE_MAP_IMAGE)
        return "Copy is done"

    def convert_tif(self):
        """
        Конвертация JPG в TIF
        """
        for image in self.name_images():
            print(image)
            _image = Image.open(
                setting.IMAGE_ORIGINAL_PATH_JPG + "\\" + image)
            name_image = image.split('.')
            _image.save(setting.IMAGE_ORIGINAL_PATH_TIF +
                        "\\" + name_image[0] + ".TIF", "TIFF")
