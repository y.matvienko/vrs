from PIL import Image
import matplotlib.pyplot as plt
import numpy as np
import setting
from cv2 import cv2


class Histogram:
    def __init__(self, fp: str):
        """
        Модуль для работы с гистограммой изображения
        fp - путь к файлу изображения
        ft - файл шаблона
        """
        print(fp)
        self.fp = fp
        self.img = cv2.imread(fp)
        self._image = Image.open(fp)

    def __get_histogram(self, image: Image, row: int, cols: int, index: int, color: str):
        """
        Получение гистрограммы
        image - изображение
        row - количество строк
        cols - количество стобцов
        index - канал
        color - цвет канала
        """
        plt.subplot(row, cols, index)
        image_array = np.array(image)
        counts, bins = np.histogram(image_array)
        plt.title('Histogram')
        plt.xlabel('Intensity value')
        plt.ylabel('Counts')
        plt.hist(bins[1:], weights=counts, color=color, label=color)
        with open(setting.PARAMETER_PATH + "\\"  + "_" + str(color) + '.txt', 'w', encoding='utf-8') \
                as file:
            file.write("COUNTS: " + str(counts) + "\n")
            file.write("BINS: " + str(bins[1:]) + "\n")
            file.write("SCORE: " + str(len(counts)) + "\n")

            sum = 0
            suma = 0

            for item_c, item_b in zip(counts, bins[1:]):
                suma += item_c * item_b
                sum += item_c

            file.write("AVERAGE: " + str(suma / sum) + "\n")  # Сред

            sum = 0
            for item in bins[1:]:
                sum += item

            average = sum / len(counts)

            file.write("AVR: " + str(average))

            sum = 0
            for item in bins[1:]:
                sum += pow(item - average, 2)

            file.write("\nDISP: " + str(sum / (len(counts) - 1)))
            file.close()

    def get_histogram(self, file: str):
        color = ('b', 'g', 'r')
        for i, col in enumerate(color):
            print(i)
            print(col)
            hist = cv2.calcHist([self.img], [i], None, [256], [0, 256])
            print(max(hist))
            plt.plot(hist, color=col, label='s')
            plt.xlim([0, 256])  # Задается лимит значений по x
            plt.ylabel('Количество пикселей')  # Задается название y
            plt.xlabel('Яркость пикселя')  # Задается название x
            plt.legend(['Blue_Channel', 'Green_Channel', 'Red_Channel'])  # Задает легенду
            plt.title("Гистограма " + str(file.split("templates")[1].split('\\')[2].split('_')[0]) + " "
                      + str(self.img.shape[0]) + "x" + str(self.img.shape[1]))  # Надает титульник
            plt.savefig(setting.IMAGE_FIGURE_SAVE_PATH + str(file.split("templates")[1].split('\\')[2].split('_')[0])
                        + "_" + str(self.img.shape[0]) + "x"
                        + str(self.img.shape[1]))  # Сохраняет изображение гистограммы

        plt.close()

    def create_rgb_hist_demo(self, img):
        h, w, c = img.shape
        rgbHist = np.zeros([16 * 16 * 16, 1], np.float32)
        bsize = 256 / 16
        for row in range(h):
            for col in range(w):
                b = img[row, col, 0]
                g = img[row, col, 1]
                r = img[row, col, 2]
                index = np.int(b / bsize) * 16 * 16 + np.int(g / bsize) * 16 + np.int(r / bsize)
                rgbHist[np.int(index), 0] += 1
        return rgbHist

    def reducea(self, start_i: int, end_i: int, start_j: int, end_j: int, step) -> None:
        img1 = cv2.imread("./static/templates/fields/1.png")
        img2 = cv2.imread("./static/templates/fields/2.png")
        img3 = cv2.imread("./static/templates/fields/3.png")
        img4 = cv2.imread("./static/templates/fields/4.png")
        img5 = cv2.imread("./static/templates/fields/5.png")
        img6 = cv2.imread("./static/templates/fields/6.png")
        # img7 = cv2.imread("./static/templates/fields/field_10x10_6.png")
        # img8 = cv2.imread("./static/templates/fields/field_10x10_7.png")
        # img9 = cv2.imread("./static/templates/fields/field_10x10_8.png")
        # img10 = cv2.imread("./static/templates/fields/field_10x10_9.png")
        # img11 = cv2.imread("./static/templates/fields/field_10x10_10.png")
        # img12 = cv2.imread("./static/templates/fields/field_10x10_11.png")

        img = Image.open(r'.\static\images\img_test.png')
        pixels = img.load()  # create the pixel map
        hist1 = self.create_rgb_hist_demo(img1)
        hist2 = self.create_rgb_hist_demo(img2)
        hist3 = self.create_rgb_hist_demo(img3)
        hist4 = self.create_rgb_hist_demo(img4)
        hist5 = self.create_rgb_hist_demo(img5)
        hist6 = self.create_rgb_hist_demo(img6)
        # hist7 = self.create_rgb_hist_demo(img7)
        # hist8 = self.create_rgb_hist_demo(img8)
        # hist9 = self.create_rgb_hist_demo(img9)
        # hist10 = self.create_rgb_hist_demo(img10)
        # hist11 = self.create_rgb_hist_demo(img11)
        # hist12 = self.create_rgb_hist_demo(img12)

        for i in range(start_i, end_i, step):
            if i >= end_i:
                break
            for j in range(start_j, end_j, step):
                if j >= end_j:
                    break
                img_crop = img.crop((i, j, i + step, j + step))
                img_crop.save("test.png")
                img_t = cv2.imread("test.png")

                hist = self.create_rgb_hist_demo(img_t)
                math1 = cv2.compareHist(hist1, hist, cv2.HISTCMP_CORREL)
                math2 = cv2.compareHist(hist2, hist, cv2.HISTCMP_CORREL)
                math3 = cv2.compareHist(hist3, hist, cv2.HISTCMP_CORREL)
                math4 = cv2.compareHist(hist4, hist, cv2.HISTCMP_CORREL)
                math5 = cv2.compareHist(hist5, hist, cv2.HISTCMP_CORREL)
                math6 = cv2.compareHist(hist6, hist, cv2.HISTCMP_CORREL)
                # math7 = cv2.compareHist(hist7, hist, cv2.HISTCMP_CORREL)
                # math8 = cv2.compareHist(hist8, hist, cv2.HISTCMP_CORREL)
                # math9 = cv2.compareHist(hist9, hist, cv2.HISTCMP_CORREL)
                # math10 = cv2.compareHist(hist10, hist, cv2.HISTCMP_CORREL)
                # math11 = cv2.compareHist(hist11, hist, cv2.HISTCMP_CORREL)
                # math12 = cv2.compareHist(hist12, hist, cv2.HISTCMP_CORREL)

                print(i, j)
                print("MATH1: " + str(math1))
                print("MATH2: " + str(math2))
                print("MATH3: " + str(math3))
                print("MATH4: " + str(math4))
                print("MATH5: " + str(math5))
                print("MATH6: " + str(math6))
                # print("MATH7: " + str(math7))
                # print("MATH8: " + str(math8))
                # print("MATH9: " + str(math9))
                # print("MATH10: " + str(math10))
                # print("MATH11: " + str(math11))
                # print("MATH12: " + str(math12))
                print("\n\n")


                if math1 < 0.3 and math2 < 0.3 and math3 < 0.3 and math4 < 0.3 and math5 < 0.3 and math6 < 0.3:
                        # math7 < 0.5 and math8 < 0.5 and math9 < 0.5 and math10 < 0.5 and math11 < 0.5 and math12 < 0.5:
                    print(i, j, True)
                    for x in range(step):
                        for y in range(step):
                            pixels[i + x, j + y] = (255, 255, 255)

        img.show()



