import os


from src import histogram
from src.cut_image import CutImage


def open_template_file(file: str):
    """
    Открытие шаблонных изображений
    file - путь к файлу
    """
    template_names = os.listdir(file)
    for template_name in template_names:
        template_files = os.listdir(os.path.join(file, template_name))
        for template_file in template_files:
            file_path = os.path.join(file, template_name + "\\" + template_file)
            hist = histogram.Histogram(file_path)
            hist.get_histogram(file_path)


def test(file: str):
    from cv2 import cv2
    hist = histogram.Histogram(file)
    img = cv2.imread(file)
    h, w, c = img.shape
    print(h, w, c)
    hist.reducea(0, w, 0, h, 20)


def test3():
    from PIL import Image

    img = Image.open(r'.\static\images\img_test.png')  # get image
    pixels = img.load()  # create the pixel map
    print(pixels[0, 0])
    print(img.size)
    for i in range(img.size[0]):  # for every pixel:
        for j in range(img.size[1]):
            if pixels[i, j] >= (78, 78, 45):  # if not black:
                pixels[i, j] = (255, 255, 255)  # change to white

    img.show()


if __name__ == '__main__':
    test(r'.\static\images\img_test.png')
    #  CutImage(r".\static\images\original_images\jpg\DSC00003_geotag.JPG").cut(59)
